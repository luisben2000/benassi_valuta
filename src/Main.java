import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    static String acronimo_valuta_estera;
    static String denaro;

    public static void main(String[] args) {
        // Esempio JSON
        try {
            InputStreamReader r = new InputStreamReader(System.in);
            BufferedReader br = new BufferedReader(r);
            System.out.println("Inserisci l'acronimo della tua valuta");
            String base = br.readLine();
            System.out.println("Inserisci l'acronimo dell' altra valuta");
            String symbol = br.readLine();
            System.out.print("Inserisci l'importo da convertire: ");
            denaro = lettura.nextLine();
            float denaro_da_cambiare = Float.parseFloat(denaro);
            GetCambioValuta data = new GetCambioValuta(base, symbol);
            Scanner lettura = new Scanner(System.in);

            try {



                String json = data.getData();

                if (json.isEmpty())
                    throw new Exception();

                /**
                 * Estrazione dei dati dal JSON ricevuto da remoto
                 */
                JsonElement jelement = new JsonParser().parse(json);

                JsonObject jobject = jelement.getAsJsonObject();
                JsonObject jaWeather = jobject.getAsJsonObject("rates");
                String val = jaWeather.get(acronimo_valuta_estera).getAsString();
                Float cambio_denaro = Float.parseFloat(val);
                Float cambio = denaro_da_cambiare * cambio_denaro;
                System.out.println("valuta in " + acronimo_valuta_estera + " " + cambio);
            } catch (Exception e) {
                System.out.println("Controlla che la valuta e l'importo siano corretti!");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}