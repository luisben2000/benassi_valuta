/**
 * Created by giovanni on 04/12/2016.
 */
public class GetCambioValuta extends HttpObject{
    private String api_key = "18eb11abe507c6b17b214f97f6ffe7a8";
    private String json_host = "http://data.fixer.io/api/";
    private String json_prefix = "http://data.fixer.io/api/latest?access_key=";
    private String url;
    private String json_data;

    public GetCambioValuta(String base,String symbol ) {
        url = json_prefix + api_key + "&base="+ base +"&symbol=" + symbol;
        setHost(json_host);
        setUrl(url);
        setMethod("GET");
        setCharset("UTF-8");
        setMethod("GET");
        setType("application/json");
    }

    @Override
    public String getData(){
        if(super.run() == true)
            return super.getData();
        return "";
    }
}