import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * @author  Giovanni Franco 05/01/2019.
 * @version 1.0.0
 */
public class HttpObject {
    private String data;
    private String host;
    private String url;
    private String charset;
    private String method;
    private String type;

    public String getData() {
        return data;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private URL server;
    private HttpURLConnection service;
    private BufferedReader input;
    private int status;
    private String line;

    /**
     *  Costruttore di default
     *
     */
    public HttpObject(
    ) {
        this.host = "";
        this.url = "";
        this.charset = "UTF-8";
        this.method = "GET";
        this.type = "application/json";
    }

    /**
     *  Costruttore
     *
     * @param host      Indirizzo del del webservice
     * @param url       URL contenete a querystring con le richieste al webservice
     * @param charset   Codifica dei caratteri
     * @param method    Metodo utilizzato per il traferimento dei dati (GET, POST, DELETE, ...)
     * @param type      Formato dei dati trasferiti
     */
    public HttpObject(String host, String url, String charset, String method, String type) {
        this.host = host;
        this.url = url;
        this.charset = charset;
        this.method = method;
        this.type = type;
    }

    /**
     *  Costruttore
     *
     * @param host      Indirizzo del del webservice
     * @param url       URL contenete a querystring con le richieste al webservice
     */
    public HttpObject(String host, String url) {
        this.host = host;
        this.url = url;
        this.charset = "UTF-8";
        this.method = "GET";
        this.type = "application/json";
    }

    public boolean run() {
        try {
            // costruzione dello URL di interrogazione del web-service
            //url = json_prefix + URLEncoder.encode(address, "UTF-8") + "&appid=" + api_key;
            server = new URL(url);
            service = (HttpURLConnection)server.openConnection();
            // impostazione header richiesta
            service.setRequestProperty("Host", host);
            service.setRequestProperty("Accept", type);
            service.setRequestProperty("Accept-Charset", charset);
            // impostazione metodo di richiesta GET
            service.setRequestMethod(method);
            // attivazione ricezione
            service.setDoInput(true);
            // connessione al web-service
            service.connect();
            // verifica stato risposta
            status = service.getResponseCode();
            if (status != 200) {
                return false;
            }
            // aperture stream di ricezione da risorsa web
            input = new BufferedReader(new InputStreamReader(service.getInputStream(), charset));
            // ciclo di lettura da web
            data = "";
            while ((line = input.readLine()) != null) {
                data += line;
            }
            input.close();
            return true;
        }
        catch (IOException e) {
            return false;
        }
    }


}